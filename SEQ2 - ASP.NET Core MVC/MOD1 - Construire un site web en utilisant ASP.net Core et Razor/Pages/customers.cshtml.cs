using System;
using System.Collections.Generic;
using Ifinfo.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;

namespace NorthwindWeb.Pages
{
    public class customers : PageModel
    {
        private Northwind db;
        public IEnumerable<Customer> Customers { set; get; }
        public List<countryWithCustomers> list { set; get; }
        
        public customers(Northwind injectedContext)
        {
            db = injectedContext;
            list = new List<countryWithCustomers>();
        }

        public void OnGet()
        {
            ViewData["Title"] = "Northwind Web Site - Customers";
            var coutries = db.Customers.Select(c => c.Country).Distinct();
            foreach (var country in coutries)
            {
                var customers = db.Customers.AsEnumerable().Where(c => c.Country == country);
                this.list.Add(new countryWithCustomers(country, customers));
            }
        }
    }

    public class countryWithCustomers
    {
        public string country { get; set; }
        public IEnumerable<Customer> customers { get; set; }

        public countryWithCustomers(string country, IEnumerable<Customer> customers)
        {
            this.country = country;
            this.customers = customers;
        }
    }
}