using System;
using System.Collections.Generic;
using System.Linq;
using Ifinfo.Shared;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace NorthwindWeb.Pages
{
    public class customer : PageModel
    {
        private Northwind db;
        public Customer selectedCustomer { get; set; }
        public IEnumerable<Order> orders { get; set; }

        public customer(Northwind injectedContext)
        {
            db = injectedContext;
        }
        
        public void OnGet()
        {
            selectedCustomer = db.Customers.AsEnumerable().Single(c => c.CustomerID == Request.Query["id"]);
            orders = db.Orders.AsEnumerable().Where(o => o.Customer == selectedCustomer);
        }
    }
}