using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using Ifinfo.Shared;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace NorthwindWeb.Pages
{
    public class SuppliersModel : PageModel
    {
        public IEnumerable<Supplier> Suppliers {get; set;}
        private Northwind db;

        [BindProperty]
        public Supplier Supplier {get; set;}

        public IActionResult OnPost()
        {
            if(ModelState.IsValid)
            {
                db.Suppliers.Add(Supplier);
                db.SaveChanges();
                return RedirectToPage("/suppliers");
            }
            return Page();
        }

        public SuppliersModel(Northwind injectedContext)
        {
            db = injectedContext;
        }
        public void OnGet()
        {
            ViewData["Title"] = "Northwind Web Site - Suppliers";
            Suppliers = db.Suppliers.ToArray();
        }
    }
}