using System;
using static System.Console;
using Ifinfo.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Storage;
using System.Xml.Linq;

namespace CorrectionTP1
{
    class Program
    {
        static void Main(string[] args)
        {
            //JoinClassesAndEleves();
            //GroupJoinClassesAndEleves();
            OutputElevesAsXml();
        }

        static void JoinClassesAndEleves()
        {
            using (var db = new GestionClasses())
            {

                var queryJoin = db.Eleves.Join(
                    inner:db.Classes,
                    outerKeySelector: eleves => eleves.ClasseID,
                    innerKeySelector : classes => classes.ClasseID,
                    resultSelector: (e, c) => new {e.NomEleve, c.NomClasse, e.EleveID}
                )
                .OrderBy(ce => ce.NomEleve);
                foreach(var item in queryJoin)
                {
                    WriteLine("{0} : {1} is in {2}.", 
                    arg0: item.EleveID,
                    arg1: item.NomEleve,
                    arg2: item.NomClasse
                    );
                }
            }
        }

        static void GroupJoinClassesAndEleves()
        {
            using (var db = new GestionClasses())
            {
                // group all eleves by their classes
                var queryGroup = db.Classes.AsEnumerable().GroupJoin(
                inner: db.Eleves,
                outerKeySelector: classes => classes.ClasseID,
                innerKeySelector: eleves => eleves.ClasseID,
                resultSelector: (c, matchingEleves) => new {
                c.NomClasse,
                Eleves = matchingEleves.OrderBy(e => e.NomEleve)
                });
                foreach (var item in queryGroup)
                {
                    WriteLine("{0} has {1} eleves.",
                    arg0: item.NomClasse,
                    arg1: item.Eleves.Count());
                    foreach (var eleve in item.Eleves)
                    {
                        WriteLine($" {eleve.NomEleve}");
                    }
                }
            }
        }

        static void OutputElevesAsXml()
        {
            using (var db = new GestionClasses())
            {
                var elevesForXml = db.Eleves.ToArray();
                var xml = new XElement("eleves",
                from e in elevesForXml
                select new XElement("eleve",
                new XAttribute("id", e.EleveID),
                new XAttribute("nom", e.NomEleve),
                new XElement("prenom", e.PrenomEleve)));
                WriteLine(xml.ToString());
            }
        }

    }
}
