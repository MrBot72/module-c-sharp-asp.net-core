using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Proxies;
namespace Ifinfo.Shared
{
    public class GestionClasses : DbContext
    {
        public DbSet<Classes> Classes{get;set;}
        public DbSet<Eleves> Eleves{get;set;}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string path = System.IO.Path.Combine(
                System.Environment.CurrentDirectory, "GestionClasses.db"
            );
            optionsBuilder.UseSqlite($"Filename={path}");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Classes>()
            .Property(classe => classe.NomClasse)
            .IsRequired()
            .HasMaxLength(45);
        }
    }
}