using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Ifinfo.Shared
{
    public class Classes
    {
        [Key]
        public int ClasseID{get;set;}
        public string NomClasse{get;set;}
        public string NiveauClasse{get;set;}
    }
}
