using System;
using Ifinfo.Shared;
using System.Data.Common;
using static System.Console;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CorrectionTP2_EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the name of a city :");
            var country = Console.ReadLine();
            GetCompanyByCity(country);
            GetUniqueCityByCustomers();
        }

        static void GetCompanyByCity(string city) {
        
            using (var db = new Northwind()) {
                
                var query = db.Customers.Where(c => c.City.Equals(city)).Select(c => new { c.CompanyName, c.CustomerID });
                Console.Write("There are {0} customers in {1} :", arg0: query.Count(), arg1: city);
                Console.WriteLine();
                foreach (var item in query) {
                    Console.WriteLine(item.CustomerID + " " + item.CompanyName);
                }
                Console.WriteLine();
            }
        }
        
        static void GetUniqueCityByCustomers() {
            using (var db = new Northwind()) {
                var query = db.Customers.Distinct().OrderBy(c => c.City).Select(c => new { c.City });
                var lastQuery = query.Last();
                foreach (var item in query) {
                    if (item.City != lastQuery.City) {
                        Console.Write("{0}, ", arg0: item.City);
                    }
                    else {
                        Console.Write("{0}.", arg0: item.City);
                    }
                }
            }
        }
    }
}
