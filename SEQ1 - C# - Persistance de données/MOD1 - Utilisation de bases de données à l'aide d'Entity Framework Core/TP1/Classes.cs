using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Ifinfo.Shared
{
    public class Classes
    {
        [Key]
        public int ClasseID{get;set;}
        public string NomClasse{get;set;}
        public string NiveauClasse{get;set;}

        public virtual ICollection<Eleves> Eleves {get;set;}
        public Classes()
        {
            this.Eleves = new HashSet<Eleves>();
        }
    }
}