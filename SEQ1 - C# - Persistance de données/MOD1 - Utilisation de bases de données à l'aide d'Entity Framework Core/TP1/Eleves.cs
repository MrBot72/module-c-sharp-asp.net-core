using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Ifinfo.Shared
{
    public class Eleves
    {
        [Key]
        public int EleveID{get;set;}
        [Required]
        [StringLength(45)]
        public string NomEleve{get;set;}
        [Required]
        [StringLength(45)]
        public string PrenomEleve{get;set;}
        public int ClasseID{get;set;}
        public virtual Classes Classe{get;set;}
    }
}
