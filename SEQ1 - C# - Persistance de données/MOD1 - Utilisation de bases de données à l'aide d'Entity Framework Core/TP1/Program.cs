using System;
using static System.Console;
using Ifinfo.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Storage;

namespace CorrectionTP1
{
    class Program
    {
        static void Main(string[] args)
        {
            QueryingClasses();
            //QueryingEleves();
            /*if(AddEleve(1, "Jean", "Michel"))
            {
                WriteLine("Ajout d'un élève avec succès");
            }*/
            /*if(UpdateNameEleve("jean", "Michel"))
            {
                WriteLine("Mise à jour de l'élève avec succès");
            }*/
            /*int deleted = DeleteEleve("Jean");
            WriteLine($"{deleted} élève(s) ont été supprimés.");
            ListEleves();*/
        }

        static void QueryingClasses()
        {
            using (var db = new GestionClasses())
            {
                WriteLine("Classes et le nombre d'élèves qu'elles contiennent : ");

                
                IQueryable<Classes> classes;
                db.ChangeTracker.LazyLoadingEnabled = false;
                Write("Activer eager loading ? (Y/N) :");
                bool eagerloading = (ReadKey().Key==ConsoleKey.Y);
                bool explicitloading = false;
                WriteLine();
                if(eagerloading)
                {
                    classes = db.Classes.Include(c => c.Eleves);
                }
                else
                {
                    classes = db.Classes;
                    Write("Activer explicit loading ? (Y/N) : ");
                    explicitloading = (ReadKey().Key==ConsoleKey.Y);
                    WriteLine();
                }

                foreach(Classes c in classes)
                {
                    if(explicitloading)
                    {
                        Write($"Chargement Explicit des élèves pour {c.NomClasse} ? (Y/N) : ");
                        ConsoleKeyInfo key = ReadKey();
                        WriteLine();
                        if(key.Key == ConsoleKey.Y)
                        {
                            var eleves = db.Entry(c).Collection(c2 => c2.Eleves);
                            if(!eleves.IsLoaded)eleves.Load();
                        }
                    }
                    WriteLine($"{c.NomClasse} has {c.Eleves.Count} élèves.");
                }
            }
        }

        static void QueryingEleves()
        {
            using (var db = new GestionClasses())
            {
                WriteLine("Eleve(s) dont le nom est similaire au prénom saisi");
                string input;
                Write("Entrez un prenom : ");
                input = ReadLine();

                IQueryable<Eleves> eleves = db.Eleves
                .Where(eleves => eleves.NomEleve.Contains(input))
                .OrderByDescending(eleves => eleves.NomEleve);

                foreach (Eleves item in eleves)
                {
                    WriteLine($"{item.EleveID} : {item.NomEleve} {item.PrenomEleve}");
                }
            }
        }

        static bool AddEleve(int classeID, string nomEleve, string prenomEleve)
        {
            using (var db = new GestionClasses())
            {
                var newEleve = new Eleves{
                    ClasseID = classeID,
                    NomEleve = nomEleve,
                    PrenomEleve = prenomEleve
                };
                db.Eleves.Add(newEleve);
                int affected = db.SaveChanges();
                return (affected == 1);
            }
        }

        static void ListEleves()
        {
            using (var db = new GestionClasses())
            {
                WriteLine("{0,-3}{1,-35}{2,8}","ID", "Nom Eleves", "Prénom Elève");
                foreach(var item in db.Eleves.OrderByDescending(e => e.NomEleve))
                {
                    WriteLine("{0:000}{1,-35}{2,8}", item.EleveID, item.NomEleve, item.PrenomEleve);
                }
            }
        }

        static bool UpdateNameEleve(string name, string newName)
        {
            using (var db = new GestionClasses())
            {
                Eleves updateEleve = db.Eleves.First(e => e.NomEleve.StartsWith(name));
                updateEleve.NomEleve = newName;
                int affected = db.SaveChanges();
                return (affected == 1);
            }
        }

        static int DeleteEleve(string name)
        {
            using (var db = new GestionClasses())
            {
                using (IDbContextTransaction t = db.Database.BeginTransaction())
                {
                    WriteLine("Niveau d'isolation de la transaction : {0}", t.GetDbTransaction().IsolationLevel);
                    IEnumerable<Eleves> eleves = db.Eleves.Where(e => e.NomEleve.StartsWith(name));
                    db.Eleves.RemoveRange(eleves);
                    int affected = db.SaveChanges();
                    return affected;
                }
            }
        }
    }
}
